/*
 render and canvas goes here 
 */

// canvas from "document"
var canvas = document.createElement("canvas");
// content from "canvas"
var ctx = canvas.getContext("2d");
// set canvas size
canvas.width = 800;
canvas.height = 600;
// adding canvas
// XXX debug comment out
document.body.appendChild(canvas);

// render mode is mode of rendering (0 by default), as follows:
// 0:start screen
// 1:in game
// 2:game over
// 3:troll mode
// 4:
// 5:

var renderMode = 0;

/*
 * images
 */

// sprite images
var spriteImages = [];
for ( var i = 0; i < 30; i++) {
	spriteImages[i] = new Image();
	spriteImages[i].loaded = true;
	spriteImages[i].index = i;
	spriteImages[i].onload = function() {
		spriteImages[index] = true;
	};
	spriteImages[i].src = "res/sprites/" + i + ".png";
}

// door images
var doorImages = [];
for ( var i = 0; i < 13; i++) {
	doorImages[i] = new Image();
	doorImages[i].loaded = true;
	doorImages[i].index = i;
	doorImages[i].onload = function() {
		doorImages[index] = true;
	};
	doorImages[i].src = "res/doors/" + (i + 1) + ".png";
}

// sand image
var sandImage = new Image();
sandImage.loaded = false;
sandImage.onload = function() {
	sandImage.loaded = true;
};
sandImage.src = "res/maze.png";

// banner image
var bannerImage = new Image();
// "is loaded" flag
bannerImage.loaded = false;
// function sets "is loaded flag" to true
bannerImage.onload = function() {
	bannerImage.loaded = true;
};
// source path
bannerImage.src = "res/panoramic.png";
// scrolling position
bannerImage.imageScrollPos = 800;
// scrolling speed
bannerImage.imageScrollSpeed = .02;

// flashing red banner text color variables
var bannerTRShade = 0;
var bannerTRChangeDelay = 2;
var bannerTRChangeCounter = 0;
var bannerTRPosNegFlag = 1;

//game over
var winner = 0;

/*
 * render function
 * 
 * takes integer for deltaTime (only used for RenderEngine controlled
 * animations)
 */
var render = function(deltaTime) {
	// return;
	// 1 ------------------------------
	if (renderMode == 0) {

		var greyShade = 50;
		ctx.fillStyle = "rgb(" + greyShade + "," + greyShade + "," + greyShade
				+ ")";
		ctx.fillRect(0, 0, 800, 600);

		bannerImage.imageScrollPos += deltaTime * bannerImage.imageScrollSpeed;
		if (bannerImage.loaded) {
			ctx.drawImage(bannerImage, bannerImage.imageScrollPos % 1600, 100);
			ctx.drawImage(bannerImage,
					(bannerImage.imageScrollPos % 1600) - 1600, 100);
		}

		ctx.textAlign = "center";
		ctx.fillStyle = "rgb(250,250,250)";
		ctx.font = "80px Arial";
		ctx.fillText("Tank Maze", 400, 80);
		ctx.textAlign = "left";

		var whiteShade = 200;
		ctx.fillStyle = "rgb(" + whiteShade + "," + whiteShade + ","
				+ whiteShade + ")";
		ctx.font = "20px Arial";
		ctx.fillText("by: Byteshift and A.J.", 550, 560);
		ctx.fillText("sites.google.com/site/byteshiftcode/",450,580)

		// red shade
		if (bannerTRChangeCounter > bannerTRChangeDelay) {
			bannerTRChangeCounter = 0;
			bannerTRShade += bannerTRPosNegFlag;
			if (bannerTRShade >= 255 || bannerTRShade <= 0) {
				bannerTRPosNegFlag *= -1;
			}
		} else {
			bannerTRChangeCounter += deltaTime;
		}

		ctx.fillStyle = "rgb(" + bannerTRShade + ",0,0)";
		ctx.font = "40px Arial";
		ctx.textAlign = "center";
		ctx.fillText("Click To Start", 400, 520);
		ctx.textAlign = "left";

		// 2 ----------------------------
	} else if (renderMode == 1) {
		// render sand
		if (sandImage.loaded) {
			ctx.drawImage(sandImage, 0, 0);
		}

		// iterate over doors in door groups and renders them if closed
		for ( var i = 0; i < mazeDoors.length; i++) {
			for ( var k = 0; k < mazeDoors[i].doors.length; k++) {
				if (!mazeDoors[i].doors[k].open
						&& doorImages[mazeDoors[i].doors[k].doorId].loaded) {
					ctx.drawImage(doorImages[mazeDoors[i].doors[k].doorId], 0,
							0);
				}
			}
		}

		// game rendering
		for ( var i = 0; i < gameEntities.length; i++) {
			if(gameEntities[i] instanceof Sparks || gameEntities[i] instanceof RaBSparks){
				var rio = gameEntities[i].getRenderInfo();
				drawSpriteImage(rio.x, rio.y, rio.rot,rio.scale,
						spriteImages[rio.imageIndex], ctx);
			}
		}
		// reg obj
		for ( var i = 0; i < gameEntities.length; i++) {
			if(!(gameEntities[i] instanceof Sparks || gameEntities[i] instanceof RaBSparks)){
				var rio = gameEntities[i].getRenderInfo();
				drawSpriteImage(rio.x, rio.y, rio.rot,rio.scale,
						spriteImages[rio.imageIndex], ctx);
			}
		}
	} else if (renderMode == 2) {
		// game over
		drawSpriteImage(400,300,0,1,spriteImages[(winner == 0)?19:20],ctx);
		ctx.fillStyle = "rgb(255,255,255)";
		ctx.font = "70px Arial";
		ctx.textAlign = "center";
		ctx.fillText(((winner == 0)?"Blue":"Red") + " won!!",400,300);
		ctx.font = "20px Arial";
		ctx.fillStyle = "rgb(200,200,200)";
		ctx.fillText("click to play again",400,375);
		ctx.textAlign = "left";

	} else if (renderMode == 3) {
		// troll mode
		ctx.fillStyle = "rgb(255,255,255)";
		ctx.fillRect(0, 0, 800, 600);
		ctx.fillStyle = "rgb(0,0,0)";
		ctx.font = "200px Arial";
		// print text
		ctx.fillText("TROLL!!!", 0, 590);
		ctx.fillText("OLLRT!!!", 0, 430);
		ctx.fillText("RLTOL!!!", 0, 270);
		ctx.fillText("LLORT!!!", 0, 110);

	} else if (renderMode == 4) {

	} else if (renderMode == 5) {

	}
};

// render images on center of origin and rotated
var drawSpriteImage = function(x, y, rot,scale, image, ctx) {
	// save the context's co-ordinate system before
	// we screw with it
	ctx.save();

	// move the origin to 50, 35
	ctx.translate(x, y);

	// rotate around this point
	ctx.rotate(rot * 3.14159 / 180);
	
	//scale
	ctx.scale(scale,scale);

	// set location so that image is centered
	var cx = image.width / 2;
	var cy = image.height / 2;

	// then draw the image back and up
	if (image.loaded) {
		ctx.drawImage(image, -cx, -cy);
	}
	// and restore the co-ordinate system to its default
	// top left origin with no rotation
	ctx.restore();
};
