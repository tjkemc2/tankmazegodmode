// door objects
function Door(doorIdIn) {
	this.bbList = [//
	new BoundBox(10, 85, 395, 0, true),//1
	new BoundBox(283,10,0,183,true),//2
	new BoundBox(205,12,303,203,true),//3
	new BoundBox(195,12,450,194,true),//4
	new BoundBox(210,12,700,166,true),//5
	new BoundBox(12,260,205,297,true),//6
	new BoundBox(12,205,400,290,true),//7
	new BoundBox(12,260,598,289,true),//8
	new BoundBox(410,10,0,420,true),//9
	new BoundBox(205,12,300,395,true),//10
	new BoundBox(212,12,500,397,true),//11
	new BoundBox(210,12,696,418,true),//12
	new BoundBox(10,85,395,515,true),//13
	];//
	this.boundBox = this.bbList[doorIdIn];
	this.open = false;
	this.setOpen = function(bool) {
		this.open = bool;
	};
	this.doorId = doorIdIn;
};
// wall group object
// takes list of doors
// takes array of possible # of doors open
function DoorGroup(doors, doorsOpenRange) {
	this.doors = doors;
	this.doorsOpenRange = doorsOpenRange;
	this.randomizeDoors = function() {
		for ( var i = 0; i < doors.length; i++) {
			doors[i].setOpen(false);
		}
		var ndopen = doorsOpenRange[Math.round(Math.random()
				* (doorsOpenRange.length - 1))];
		for ( var i = 0; i < ndopen; i++) {
			var rn = Math.round(Math.random() * (doors.length - 1));
			if (!doors[rn].open) {
				doors[rn].setOpen(true);
			} else {
				i--;
				continue;
			}
		}
	};
};