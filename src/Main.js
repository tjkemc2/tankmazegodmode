// version a_1.0.1

/*
 * DeltaTimer is used for calculating delta time;
 */
// variables -----
// deltaTime variables
var deltaTime = 0;
var oldTime = 0;
var newTime = 0;

// FUE flag
var isFirstTime = true;

// game over delay
var gameOverDelayMax = 8000;
var gameOverDelayC = gameOverDelayMax;

// game running flag
var gameRunning = false;

// The main game loop
// is called in a loop with "setInterval" below
function main() {
	// update delta time
	newTime = Date.now();
	deltaTime = newTime - oldTime;
	//speed up time
	deltaTime *= 2;
	// do game loop functions here that need "deltaTimer.getDeltaTime();"
	if (gameRunning) {
		tickAllGameEntities(deltaTime);
	}
	if (renderMode == 2) {
		gameOverDelayC -= deltaTime;
		if (gameOverDelayC < 0) {
			gameOverDelayC = gameOverDelayMax;
			renderMode = 0;
		}
	}
	render(deltaTime);
	// ------------------------------------------------------------------
	oldTime = Date.now();
};

// set old time
oldTime = Date.now();
setInterval(main, 1); // Execute as fast as possible
// debug call main once
// main();

// function listener
addListener(function(args) {
	if ((args[0] == "mouseClick" || ((args[0] == "keyDown" && args[1] == "32")))
			&& (renderMode == 0 || renderMode == 2)) {
		renderMode = 1;
		gameRunning = true;
		reset();
	}
	if (args[0] == "keyDown" && args[1] == "32") {
		// XXX debug
		// gameRunning = false;
		// renderMode = 2;
		// winner = 1;
	}
	if (args[0] == "gameOver") {
		gameRunning = false;
		renderMode = 2;
		winner = args[1];
		gameOverDelayC = gameOverDelayMax;
	}
});
// reset function
function reset() {
	// clear game entities
	gameEntities.length = 0;
	// add tanks
	spawnTank(0);
	spawnTank(1);
	randAllDoors();
	if (isFirstTime) {
		gameEntities.push(new Fue());
		isFirstTime = false;
	}
	gameEntities.push(new CP(0));
	gameEntities.push(new CP(1));
}
