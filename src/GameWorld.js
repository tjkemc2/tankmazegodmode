// door tick's variables
var mdtCount = 0;
var mdtMax = 5000;//5000 to 6000 ??? maybe

// remove function for array
// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

// tick all entities
function tickAllGameEntities(deltaTime) {
	// calls update collision box
	for ( var i = 0; i < gameEntities.length; i++) {
		gameEntities[i].updateBoundBox();
	}
	// calls tick on every gameobject in game entities
	for ( var i = 0; i < gameEntities.length; i++) {
		if(gameEntities[i].toRemove){
			gameEntities.remove(i);
		}
		gameEntities[i].tick(deltaTime);
	}
	
	if(mdtCount > mdtMax){
		mdtCount = 0;
		randAllDoors();
	}else{
		mdtCount += 1;
	}
};

function randAllDoors(){
	for ( var i = 0; i < mazeDoors.length; i++) {//and, for every door in mazedoors: randomize the doors
		mazeDoors[i].randomizeDoors();
	}
};

// doors
var mazeDoors = [// mazeDoors
new DoorGroup(// dg#1
[ new Door(0) ],// doors
[ 0, 1 ]// #'s
),// end dg#1
new DoorGroup(//
[ new Door(12) ],//
[ 0, 1 ]//
), //
new DoorGroup(//
[ new Door(1), new Door(5), new Door(8) ],//
[ 1, 2 ]//
), //
new DoorGroup(//
[ new Door(2), new Door(9) ],//
[ 1, 2 ]//
), //
new DoorGroup(//
[ new Door(3), new Door(10) ],//
[ 1, 2 ]//
), //
new DoorGroup(//
[ new Door(6) ],//
[ 0, 1 ]//
), //
new DoorGroup(//
[ new Door(4), new Door(11), new Door(7) ],//
[ 1, 2 ]//
) //
];// end mazeDoors

// event management
var actionListeners = [];
// add to action listener
function addListener(functionToAdd) {
	actionListeners.push(functionToAdd);
};

// register action function
// takes an array of args
function registerAction(args) {
	for ( var i = 0; i < actionListeners.length; i++) {
		actionListeners[i](args);
	}
	// notify all game objects
	for(var i = 0; i < gameEntities.length; i++){
		gameEntities[i].listenToAction(args);
	}
};

// code to sense clicking
document.querySelector("canvas").addEventListener("click", function() {
	registerAction( [ "mouseClick" ]);
});

// code to sense space bar
/*
 * $(document).bind("keydown", "space", function() { registerAction( [
 * "keyDown", "space" ]); });
 */
// kappy's way
document.onkeydown = function(e) {
	registerAction( [ "keyDown", e.keyCode ]);
};
document.onkeyup = function(e) {
	registerAction( [ "keyUp", e.keyCode ]);
};

addListener(function(args) {

});

// spawn tanks
// color:0 = left,1 = right
function spawnTank(color) {
	gameEntities.push(new Tank(color));
}
