function GameObject() {
	// colision detection
	this.boundBox = new BoundBox(0, 0, 0, 0, false);
	this.updateBoundBox = function() {
		this.boundBox.x = this.x;
		this.boundBox.y = this.y;
	};
	// location and rotation
	this.x = 0;
	this.y = 0;
	this.rot = 0;
	// called to update object
	this.tick = function(deltaTime) {
	};
	/*
	 * returns RenderInfoObject that contains rendering info
	 */
	this.getRenderInfo = function() {
	};
	this.listenToAction = function() {
	};
	this.toRemove = false;
};

function BoundBox(w, h, x, y, fromCorner) {
	this.w = w;
	this.h = h;
	this.x = x;
	this.y = y;
	this.fromCorner = fromCorner;
	this.isTouching = function(bb) {
		if (!fromCorner) {
			return (Math.abs((this.x - this.w / 2) - (bb.x - bb.w / 2)) * 2 < (this.w + bb.w))
					&& (Math.abs((this.y - this.h / 2) - (bb.y - bb.h / 2)) * 2 < (this.h + bb.h));
		} else {
			return (Math.abs(this.x - (bb.x - bb.w / 2)) * 2 < (this.w + bb.w))
					&& (Math.abs(this.y - (bb.y - bb.h / 2)) * 2 < (this.h + bb.h));
		}
	};
};

// tests for bounding box
/*
 * var bb1 = new BoundBox(10, 10, 0, -10); var bb2 = new BoundBox(10, 10, 0, 0);
 * 
 * //true bb1 = new BoundBox(10,10,0,2); alert(bb1.isTouching(bb2)); // false
 * bb1 = new BoundBox(10,10,0,10); alert(bb1.isTouching(bb2)); // false bb1 =
 * new BoundBox(10, 10, 0, -10); alert(bb1.isTouching(bb2)); // true bb1 = new
 * BoundBox(10, 10, 0, -2); alert(bb1.isTouching(bb2)); // false bb1 = new
 * BoundBox(10,10,10,0);alert(bb1.isTouching(bb2));
 * 
 * //true bb1 = new BoundBox(10,10,2,0);alert(bb1.isTouching(bb2));
 * 
 * //false bb1 = new BoundBox(10,10,-10,0);alert(bb1.isTouching(bb2));
 * 
 * //true bb1 = new BoundBox(10,10,-2,0);alert(bb1.isTouching(bb2));
 */

function RenderInfoObject(x, y, rot, scale, imageIndex) {
	this.x = x;
	this.y = y;
	this.rot = rot;
	this.scale = scale;
	this.imageIndex = imageIndex;
};

function Tank(color) {
	// location
	this.x = (color == 0) ? 50 : 750;
	this.y = (Math.random() > .5) ? 50 : 550;
	this.rot = 180 * color;
	// missile spawn delay
	this.missileSpawnDelay = 1250;
	this.missileSpawnCounter = 0;
	//
	this.hurt = function() {
		this.x = (color == 0) ? 50 : 750;
		this.y = (Math.random() > .5) ? 50 : 550;
		this.rot = 180 * color;
		this.missileSpawnCounter = 100;
	};
	this.boundBox = new BoundBox(40, 40, 0, 0);
	this.color = color;
	this.imageIndex = (color == 0) ? /* blue */4 : /* red */5;
	// key map
	// [for,back,left,right]
	this.keyMap = (color == 0) ? /* blue=wsad */[ 87, 83, 65, 68, 81, 69 ]
			: /* red=ikjl */[ 73, 75, 74, 76, 85, 79 ];
	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, 1,
				this.imageIndex);
	};
	this.shoot = false;
	// forwardMovementMode
	// 0:nothing
	// 1:forward
	// -1:backwards
	this.forwardMovementMode = 0;
	// turningmovementmode
	// 0: nothing
	// -1: CCW
	// 1:CW
	this.turningMovementMode = 0;
	this.listenToAction = function(args) {
		// for + back
		if (args[0] == "keyDown") {
			if (args[1] == this.keyMap[4] || args[1] == this.keyMap[5]) {
				this.shoot = true;
			}
			if (args[1] == this.keyMap[0]) {
				this.forwardMovementMode = 1;
			} else if (args[1] == this.keyMap[1]) {
				this.forwardMovementMode = -1;
			}
			// side to side
			if (args[1] == this.keyMap[2]) {
				this.turningMovementMode = -1;
			} else if (args[1] == this.keyMap[3]) {
				this.turningMovementMode = 1;
			}
		}
		if (args[0] == "keyUp") {
			if (args[1] == this.keyMap[4] || args[1] == this.keyMap[5]) {
				this.shoot = false;
			}
			if (args[1] == this.keyMap[0]) {
				if (this.forwardMovementMode == 1) {
					this.forwardMovementMode = 0;
				}
			} else if (args[1] == this.keyMap[1]) {
				if (this.forwardMovementMode == -1) {
					this.forwardMovementMode = 0;
				}
			}
			// side to side
			if (args[1] == this.keyMap[2]) {
				if (this.turningMovementMode == -1) {
					this.turningMovementMode = 0;
				}
			} else if (args[1] == this.keyMap[3]) {
				if (this.turningMovementMode == 1) {
					this.turningMovementMode = 0;
				}
			}
		}
	};
	this.tick = function(deltaTime) {

		// if shooting, fire missile
		if (this.shoot && this.missileSpawnCounter < 0) {
			gameEntities.push(new Missile(this.x, this.y, this.rot));
			this.missileSpawnCounter = this.missileSpawnDelay;
		}
		this.missileSpawnCounter -= deltaTime;

		this.rot += this.turningMovementMode * .1 * deltaTime;
		this.boundBox.x += Math.cos(this.rot * 3.14159 / 180)
				* this.forwardMovementMode * .1 * deltaTime;
		this.boundBox.y += Math.sin(this.rot * 3.14159 / 180)
				* this.forwardMovementMode * .1 * deltaTime;

		// check colision and move back if touching
		for ( var i = 0; i < mazeDoors.length; i++) {
			for ( var k = 0; k < mazeDoors[i].doors.length; k++) {
				if (!mazeDoors[i].doors[k].open && mazeDoors[i].doors[k].boundBox.isTouching(this.boundBox)) {
					this.boundBox.x = this.x;
					this.boundBox.y = this.y;
					// spawn sparks
					gameEntities
							.push(new Sparks(this.x + 30 * Math.random()
									* (Math.random() < .5 ? -1 : 1), this.y
									+ 30 * Math.random()
									* (Math.random() < .5 ? -1 : 1), .3));
					return;
				}
			}
		}
		// check if off screen
		if(this.boundBox.x < 0 || this.boundBox.y < 0 || this.boundBox.x > 800 || this.boundBox.y > 600){
			this.boundBox.x = this.x;
			this.boundBox.y = this.y;
			// spawn sparks
			gameEntities
					.push(new Sparks(this.x + 30 * Math.random()
							* (Math.random() < .5 ? -1 : 1), this.y
							+ 30 * Math.random()
							* (Math.random() < .5 ? -1 : 1), .3));
			return;
		}

		// move
		this.x = this.boundBox.x;
		this.y = this.boundBox.y;
	};
};
Tank.prototype = new GameObject();
// first user experience
function Fue() {
	this.constructor = new function() {
	};
	this.fueImages = [ 6, 7 ];
	this.ii = 0;
	this.getRenderInfo = function() {
		return new RenderInfoObject(400, 10, 0, 1, this.fueImages[this.ii]);
	};
	this.life = 1000;
	this.imSwap = 1000;
	this.tick = function(deltaTime) {
		if (this.ii == 0) {
			this.imSwap--;
			if (this.imSwap < 0) {
				this.ii = 1;
				gameEntities.push(new NoticeBuble(103, 300));
				gameEntities.push(new NoticeBuble(692, 300));
			}
		}
		if (this.ii == 1) {
			this.life--;
			if (this.life < 0) {
				this.toRemove = true;
			}
		}
	};
};
Fue.prototype = new GameObject();
function NoticeBuble(x, y) {
	this.x = x;
	this.y = y;
	this.life = 1100;
	this.tick = function(deltaTime) {
		this.y -= deltaTime * .01;
		this.life -= deltaTime;
		if (this.life < 0) {
			this.toRemove = true;
		}
	};
	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, 0, 1, 2);
	};
};
NoticeBuble.prototype = new GameObject();
function Missile(x, y, rot) {
	this.armedCounter = 100;
	this.boundBox = new BoundBox(20, 20, 0, 0, false);
	this.x = x;
	this.y = y;
	this.rot = rot;
	this.speed = 1;
	this.tick = function(deltaTime) {

		this.armedCounter -= deltaTime;

		// col
		for ( var i = 0; i < mazeDoors.length; i++) {
			for ( var k = 0; k < mazeDoors[i].doors.length; k++) {
				if (!mazeDoors[i].doors[k].open
						&& mazeDoors[i].doors[k].boundBox
								.isTouching(this.boundBox)) {
					this.toRemove = true;
					gameEntities.push(new Explosion(this.x, this.y));
					return;
				}
			}
		}
		if(this.x < 0 || this.x > 800 || this.y < 0 || this.y > 600){
			this.toRemove = true;
			gameEntities.push(new Explosion(this.x, this.y));
			return;
		}
		for ( var kx = 0; kx < gameEntities.length; kx++) {
			if (gameEntities[kx].boundBox.isTouching(this.boundBox)
					&& !(gameEntities[kx] instanceof Missile || gameEntities[kx] instanceof CP)) {
				if (this.armedCounter < 0) {
					this.toRemove = true;
					gameEntities.push(new Explosion(this.x, this.y));
					return;
				}
			}
		}

		// move
		this.x += Math.cos(rot * 3.14159 / 180) * this.speed * deltaTime;
		this.y += Math.sin(rot * 3.14159 / 180) * this.speed * deltaTime;
	};
	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, 1, 0);
	};
};
Missile.prototype = new GameObject();
function Explosion(x, y) {
	this.boundBox = new BoundBox(70, 70, 0, 0, false);
	this.x = x;
	this.y = y;
	this.rot = Math.round(Math.random() * 360);
	this.iiCounter = 0;
	this.ii = 0;
	this.ims = [ 8, 9, 10, 11, 12 ];
	this.flag_hasMadeSmoke = false;
	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, 1,
				this.ims[this.ii]);
	};
	this.tick = function(deltaTime) {

		// harm tanks
		for ( var kx = 0; kx < gameEntities.length; kx++) {
			if (gameEntities[kx].boundBox.isTouching(this.boundBox)
					&& gameEntities[kx] instanceof Tank) {
				gameEntities[kx].hurt();
			}
		}

		//
		if (!this.flag_hasMadeSmoke) {
			this.flag_hasMadeSmoke = true;
			for ( var i = 0; i < 2; i++) {
				gameEntities.push(new Smoke(this.x + Math.random() * 20, this.y
						+ Math.random() * 20, 3 * Math.random()));
			}
		}
		this.iiCounter += deltaTime;
		if (this.iiCounter > 50) {
			this.iiCounter = 0;
			if (this.ii > this.ims.length - 2) {
				this.toRemove = true;
			} else {
				this.ii++;
			}
		}
	};
};
Explosion.prototype = new GameObject();
function Smoke(x, y, size) {
	this.x = x;
	this.y = y;
	this.rot = Math.round(Math.random() * 360);
	this.size = size;

	this.tick = function(deltaTime) {
		this.size -= .001 * deltaTime;
		this.y -= .01 * deltaTime;
		this.toRemove = (this.size <= 0);
	};

	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, this.size, 13);
	};
};
Smoke.prototype = new GameObject();
function Sparks(x, y, size) {
	this.x = x;
	this.y = y;
	this.rot = Math.round(Math.random() * 360);
	this.size = size;
	this.life = 300;

	this.tick = function(deltaTime) {
		this.life -= deltaTime;
		this.toRemove = (this.life < 0);
		this.y += .02 * deltaTime;
	};

	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, this.size, 16);
	};
};
Sparks.prototype = new GameObject();
function RaBSparks(x, y, color, size) {
	this.x = x;
	this.y = y;
	this.rot = Math.round(Math.random() * 360);
	this.size = size;
	this.ii = (color == 0) ? 17 : 18;
	this.life = 50;
	this.getRenderInfo = function() {
		return new RenderInfoObject(this.x, this.y, this.rot, this.size,
				this.ii);
	};
	this.life = 300;

	this.tick = function(deltaTime) {
		this.life -= deltaTime;
		this.toRemove = (this.life < 0);
		this.y -= .02 * deltaTime;
	};
};
RaBSparks.prototype = new GameObject();
function CP(color) {
	this.color = color;
	this.updateBoundBox = function() {
		// do nothing
	};
	this.boundBox = (color == 0) ? new BoundBox(92, 92, 55 + 92, 255 + 92, true)
			: new BoundBox(92, 92, 648 + 92, 255 + 92, true);
	this.getRenderInfo = function() {
		return new RenderInfoObject(0, 0, 0, 0, 0);
	};
	this.maxLife = 15000;
	this.life = this.maxLife;
	this.tick = function(deltaTime) {
		this.cap = false;
		for ( var i = 0; i < gameEntities.length; i++) {
			if (gameEntities[i].boundBox.isTouching(this.boundBox)
					&& (gameEntities[i] instanceof Tank)
					&& !(gameEntities[i].color == this.color)) {
				if(Math.random()<.20){
					gameEntities.push(new RaBSparks(this.boundBox.x
						- this.boundBox.w * Math.random(), this.boundBox.y
						- this.boundBox.h * Math.random(), this.color,
						(this.life / this.maxLife) * 1.5 * Math.random()));
				}
				this.life -= deltaTime;
			}
		}
		if (this.life < 0) {
			registerAction( [ "gameOver", (this.color == 0) ? 1 : 0 ]);
		}
	};
};
CP.prototype = new GameObject();
